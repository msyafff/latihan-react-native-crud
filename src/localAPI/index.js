import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
  StatusBar,
} from 'react-native';

// json-server --host localhost database.json --port 3004

const Item = ({
  name,
  dateOfBirth,
  email,
  occupation,
  alamat,
  onPress,
  onDelete,
}) => {
  //   const axios = require('axios').default;

  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 5,
        paddingVertical: 5,
        borderBottomWidth: 0.5,
      }}>
      <View style={{flexDirection: 'row'}}>
        <TouchableOpacity onPress={onPress}>
          <Image
            source={{
              uri:
                'https://cdn.discordapp.com/attachments/767967052983042048/788337429420441640/IMG_20200908_175732_049.jpg',
            }}
            style={{
              height: 100,
              width: 100,
              borderRadius: 50,
            }}
          />
        </TouchableOpacity>
        <View style={{marginLeft: 10, flexDirection: 'column', width: '65%'}}>
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.descName}>{name}</Text>
            <Text style={styles.dateOfBirth}>({dateOfBirth})</Text>
          </View>
          <Text style={styles.descEmail}>{email}</Text>
          <Text style={styles.descOccupation}>{occupation}</Text>
          <Text style={styles.descAlamat}>{alamat}</Text>
        </View>
      </View>
      <View style={{alignSelf: 'center'}}>
        <TouchableOpacity onPress={onDelete}>
          <Text
            style={{
              color: 'red',
              fontWeight: 'bold',
              fontSize: 24,
            }}>
            X
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const LocalAPI = () => {
  const [name, setName] = useState('');
  const [dateOfBirth, setDateOfBirth] = useState('');
  const [email, setEmail] = useState('');
  const [occupation, setOccupation] = useState('');
  const [alamat, setAlamat] = useState('');
  const [users, setUsers] = useState([]);
  const [button, setButton] = useState('Save');
  const [selectedUser, setSelectedUser] = useState({});

  useEffect(() => {
    console.log('Refresh');
    getData();
  }, []);

  const localUrl = 'http://localhost:3004/users';

  const submit = () => {
    const data = {
      name,
      dateOfBirth,
      email,
      occupation,
      alamat,
    };

    if (button === 'Save') {
      // Create RUD (POST)
      axios.post(localUrl, data).then(res => {
        console.log('response (POST): ', res);
        setName('');
        setDateOfBirth('');
        setEmail('');
        setOccupation('');
        setAlamat('');
        getData();
      });
    } else if (button === 'Update') {
      // CR Update D (PUT or PATCH?)
      axios
        .put(`http://localhost:3004/users/${selectedUser.id}`, data)
        .then(res => {
          console.log('Patch/Update: ', res);
          setName('');
          setDateOfBirth('');
          setEmail('');
          setOccupation('');
          setAlamat('');
          getData();
          setButton('Save');
        });
    }
  };

  const getData = () => {
    // C Read U D (GET)
    axios.get(localUrl).then(res => {
      console.log('result (GET) : ', res);
      setUsers(res.data);
    });
  };

  const selectedItem = item => {
    console.log('Selected Item : ', item);
    setSelectedUser(item);
    setName(item.name);
    setDateOfBirth(item.dateOfBirth);
    setEmail(item.email);
    setOccupation(item.occupation);
    setAlamat(item.alamat);
    setButton('Update');
  };

  const deleteItem = item => {
    // C R U Delete (DELETE)
    axios.delete(`http://localhost:3004/users/${item.id}`).then(result => {
      console.log('result (DELETE) : ', result);
      getData();
    });
  };

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="transparent" barStyle="dark-content" />
      <Text style={styles.title}>CRUD with LOCAL API (JSON)</Text>
      <Text style={styles.heading}>Fill the form below!</Text>
      <View style={{marginBottom: 15}}>
        <TextInput
          style={styles.input}
          placeholder="Name"
          value={name}
          onChangeText={value => setName(value)}
        />
        <TextInput
          style={styles.input}
          placeholder="Date of Birth (MM/DD/YY)"
          value={dateOfBirth}
          onChangeText={value => setDateOfBirth(value)}
        />
        <TextInput
          style={styles.input}
          placeholder="Email"
          value={email}
          onChangeText={value => setEmail(value)}
        />
        <TextInput
          style={styles.input}
          placeholder="Occupation"
          value={occupation}
          onChangeText={value => setOccupation(value)}
        />
        <TextInput
          style={styles.input}
          placeholder="Address"
          value={alamat}
          onChangeText={value => setAlamat(value)}
        />
      </View>
      <Button
        title={button}
        onPress={() => {
          name === '' ||
          dateOfBirth === '' ||
          email === '' ||
          occupation === '' ||
          alamat === ''
            ? Alert.alert(
                'Warning',
                'Please enter valid information!',
                [{text: 'Close', onPress: () => console.log('Warned!')}],
                {cancelable: true},
              )
            : submit();
        }}
      />
      <ScrollView
        style={{
          borderTopWidth: 0.5,
          borderColor: 'black',
          width: '100%',
          marginTop: 20,
        }}>
        {users.map(user => {
          return (
            <Item
              key={user.id}
              name={user.name}
              dateOfBirth={user.dateOfBirth}
              email={user.email}
              occupation={user.occupation}
              alamat={user.alamat}
              onPress={() => selectedItem(user)}
              onDelete={() =>
                Alert.alert(
                  'Caution !',
                  'Are you sure want to delete this item ?',
                  [
                    {text: 'No', onPress: () => console.log('Cancel')},
                    {text: 'Yep', onPress: () => deleteItem(user)},
                  ],
                )
              }
            />
          );
        })}
      </ScrollView>
    </View>
  );
};

export default LocalAPI;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 24,
    backgroundColor: '#f4f4f4',
  },
  title: {
    fontSize: 24,
    textAlign: 'center',
  },
  heading: {
    margin: 10,
    fontSize: 16,
    textAlign: 'center',
  },
  input: {
    borderWidth: 0.5,
    borderRadius: 5,
    marginVertical: 10,
    padding: 10,
  },
  descName: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  dateOfBirth: {
    fontSize: 12,
    alignSelf: 'center',
    marginHorizontal: 10,
    color: 'rgba(0,0,0,0.5)',
  },
  descEmail: {
    fontSize: 14,
    color: 'blue',
  },
  descOccupation: {
    fontSize: 16,
  },
  descAlamat: {
    fontSize: 14,
    color: 'rgba(0,0,0, 0.75)',
    marginTop: 5,
  },
});
