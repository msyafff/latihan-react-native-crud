import React from 'react';
import {StyleSheet} from 'react-native';
import LocalAPI from './src/localAPI';

export default function App() {
  return <LocalAPI />;
}

const styles = StyleSheet.create({});
